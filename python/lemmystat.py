# Uses lemmy api to grab all of a user's comments and show how/when
# they are active

import json
import requests
import pandas as pd
import matplotlib.pyplot as plt

# Change these as needed
username = 'neurospice'
server = 'https://lemmy.dbzer0.com'

# Starting page
page = 1
headers = {"accept": "application/json"}
url = server + '/api/v3/user?username=' + username + '&limit=50&sort=New&page='

def getResponse(page: int):
    return json.loads(requests.get(url + str(page), headers=headers).text)

data = getResponse(page)

comment_dates = []

while data['comments']:
    comment_dates += list(map(lambda cmt: cmt['comment']['published'], data['comments']))
    page += 1
    data = getResponse(page)


df = pd.DataFrame(index=comment_dates)
df.index = pd.to_datetime(df.index)

df['comments'] = df.index.value_counts()

print(f"{username}'s total comments: {df['comments'].sum()}") # Sum

df.resample('w').sum().plot(title='Weekly Comments on lemmy')
plt.savefig('plot.png')
plt.show()
