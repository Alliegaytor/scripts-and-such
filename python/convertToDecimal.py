# Converts binary to decimal

def toDec(binary, decimal, n):
    """ Calculates the decimal value of a binary value

    Input: Binary value (list), decimal (int), n (int)
    Output: Prints decimal value for given binary value

    For Example:
    >>> toDec(['1', '0', '1', '1', '0'], 0, 0)
    22
    """
    decimal += int(binary[-1]) * (2 ** n)
    del binary[-1]
    if len(binary) == 0:
        return f"Decimal: {decimal}\n"
    return toDec(binary, decimal, n + 1)


def grabInput(prompt):
    """ Asks user to input a positive integer based on the prompt given
    """
    print(prompt)
    binary = input("Binary : ")
    while True:
        allowed = "10"
        if all(c in allowed for c in binary):
            return list(binary)
        else:
            print(f"\n{binary} is not a binary value. Please try again")
            return grabInput(prompt)


if __name__ == "__main__":
    while True:
        binary = grabInput("Please input the desired binary value to be converted into decimal")
        print(toDec(binary, 0, 0))
