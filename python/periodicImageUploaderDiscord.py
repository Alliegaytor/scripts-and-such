"""
A very simple discord bot that uploads a photo in a set interval.
"""

from discord.ext import commands, tasks
from os import listdir
from random import choice
from time import strftime
import discord


# Return current time (for debugging)
def ct():
    return f"[{strftime('%H:%M:%S')}]"


# Channel id
channelid = 0123456789

# Channel name (will only try this if value is not None)
channelname = None

# Image directory
imagedirectory = 'images/'

bot  = discord.Client()


# Loop through upload every x seconds (default 15min)
@tasks.loop(seconds=900)
async def upload():
    print(f"{ct()} Posting a Random Image.")
    image = choice(listdir(imagedirectory)) # Choose random image
    path = f"{imagedirectory}{image}"
    print(f"{ct()} image path: {path}")
    await channel.send(file=discord.File(path)) # Upload image

# Prints information once bot connects to discord
@bot.event
async def on_ready():
    global channel
    print(f"{ct()} Logged in as")
    print(f"{ct()} Name: {bot.user.name}")
    print(f"{ct()} Id: {bot.user.id}")
    # Check to see if it should use channelid or channelname
    if channelname == None:
        channel = bot.get_channel(channelid) # Use channel id
    else:
        for guild in bot.guilds:
            for channels in guild.channels:
                if str(channels) == channelname:
                    channel = channels # Use chanel with channelname
                    break
    upload.start() # Start loop


# Put your token here
bot.run()
