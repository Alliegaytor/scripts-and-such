#!/usr/bin/env python
"""
Author: Alliegaytor

Prints weather data from pirateweather for waybar to parse.
Requires API key: http://pirateweather.net/en/latest/
"""

import json
from datetime import datetime
from dateutil import tz
import requests
import emoji

# https://api.pirateweather.net/forecast/[apikey]/[latitude],[longitude]

constants = {
    "url": "https://api.pirateweather.net/forecast/",
    "apikey": "",
    "latitude": "-38",
    "longitude": "150",

    "hourLim": 6,
    "dayLim": 6,
    "maxWidth": 38,
}

excluded = ("minutely", "alerts")

parameters = {
    "units": "si"
}

from_zone = tz.gettz('UTC')
to_zone = tz.gettz('Australia/Melbourne')

data = {}

icons = {
    'clear-day': '☀️', # works
    'clear-night': '🌙', # works
    'rain': '🌧️', # works
    'snow': '🌨️',
    'sleet': '☀️',
    'wind': '☀️',
    'fog': '🌫️',
    'cloudy': '☁️', # works
    'partly-cloudy-day': '🌤️', # works
    'partly-cloudy-night': '☁️' # works
}

url = constants['url'] + constants['apikey'] + "/" + constants['latitude'] + "," + constants['longitude']

# Exclude
exclude_count = len(excluded)

if exclude_count > 0:
    url = url + "?exclude="

    for index in range(0, exclude_count):
        if index == exclude_count - 1:
            url += excluded[index]
            break
        url += excluded[index] + ","


# Parameter
for parameter in parameters:
    url += "&" + parameter + "=" + parameters[parameter]


weather = requests.get(url, timeout=10).json()

# Calculate compass direction. Don't question it.
windBearing = float(weather['currently']['windBearing'])

def bearingToCompass(angle: float) -> str:
    """
    Return compass direction of angle
    """
    if angle > 22.5:
        if angle > 67.5:
            if angle > 112.5:
                if angle > 157.5:
                    if angle > 202.5:
                        if angle > 247.5:
                            if angle > 292.5:
                                if angle > 337.5:
                                    return "N"
                                return "NW"
                            return "W"
                        return "SW"
                    return "S"
                return "SE"
            return "E"
        return "NE"
    return "N"


# Format time
def formatTime(utctime: int, unit: str) -> str:
    """
    Convert and return UTC timestamp to formatted time
    """
    if unit == "day":
        timeFormat = '%m-%d'
    if unit == "hour":
        timeFormat = '%H:%M'
    return datetime.utcfromtimestamp(utctime).replace(tzinfo=from_zone).astimezone(to_zone).strftime(timeFormat)


def formatString(start: str, end: str) -> str:
    """
    Format and return two strings to have width maxWidth when joined
    """
    startEmoji = emoji.distinct_emoji_list(start)
    endEmoji = emoji.distinct_emoji_list(end)
    # Calculate how much padding is needed to seperate the strings
    padding = (
        # Calculate difference of width of string and maxWidth
        constants["maxWidth"] - len(start) - len(end)
        # Remove emoji widths as they are wildy inconsistent
        + len(''.join(startEmoji)) + len(''.join(endEmoji))
        # Assume emojis are 2 width and add them back
        - len(startEmoji) * 2 - len(endEmoji) * 2
    )

    if padding <= 0:
        return start + end
    return start + "." * padding + end


# Main text
data['text'] = icons[weather['currently']['icon']] + " " + str(weather['currently']['temperature']) + "°C"

# Secondary text
data['tooltip'] = f"<b>{weather['currently']['summary']} {round(weather['currently']['temperature'])}°C </b>\n"

data['tooltip'] += formatString("Apparent Temperature:", f"{round(weather['currently']['apparentTemperature'])}°C\n")
data['tooltip'] += formatString("Humidity:", f"{weather['currently']['humidity'] * 100}%\n")
data['tooltip'] += formatString("Wind Speed:", f"{weather['currently']['windSpeed']}m/s\n")
data['tooltip'] += formatString("Wind Bearing:", f"{windBearing}° {bearingToCompass(windBearing)}\n")
data['tooltip'] += "---------------\n"


# Hours
for hours in weather['hourly']['data'][:constants["hourLim"]]:
    time = formatTime(hours['time'], "hour")
    data['tooltip'] += formatString(f"{time} {icons[hours['icon']]}", f"{hours['summary']} {round(hours['temperature'])}°C\n")

data['tooltip'] += "---------------\n"


# Days
for days in weather['daily']['data'][:constants["dayLim"]]:
    time = formatTime(days['time'], "day")
    data['tooltip'] += formatString(f"{time} {icons[days['icon']]}", f"{days['summary']} {round(days['temperatureHigh'])}°C / {round(days['temperatureLow'])}°C\n")

print(json.dumps(data))
