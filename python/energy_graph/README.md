# Python

---

These are the scripts I use to track my home's energy usage. I've included [energy.csv](https://gitlab.com/Alliegaytor/scripts-and-such/-/blob/master/python/energy_graph/energy.csv) which is the every second day resampling of my regular csv file. I've also included a snippet of the actual csv file I get, [energy_format2.csv](https://gitlab.com/Alliegaytor/scripts-and-such/-/blob/master/python/energy_graph/energy_format2.csv).

---

### [energycsvConcat.py](https://gitlab.com/Alliegaytor/scripts-and-such/-/blob/master/python/energy_graph/energycsvConcat.py)
Concats two csv files from my energy provider.
### [energy.py](https://gitlab.com/Alliegaytor/scripts-and-such/-/blob/master/python/energy_graph/energy.py)
Outputs graphs of my energy usage from my provider's csv file. Outputs max/min/mean usage.

Here are some examples of the graphs I get:
![](2023-04-18-line_graph.png)
![](2023-04-18-scatter_graph_interpolated.png)
![](2023-04-18-scatter_graph_interpolated_smooth.png)
