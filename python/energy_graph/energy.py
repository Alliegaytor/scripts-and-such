""" Energy Python
A small script I use to graph my home energy usage.
It gives you some stats about your usage, and graphs it.

25-09-2023 Allison

Example Output:

The sample size is 7D

Most Recent Energy Usage
             read
date             
2023-09-09  12.53
2023-09-16  12.24
2023-09-23  14.48

             read
date             
2022-02-12  12.78
2022-02-19  12.71
2022-02-26  11.09

Maximum : 16 July, 2022, 21.1kWh was used
Minimum : 12 March, 2022, 3.19kWh was used
Mean    : An average of 14.24kWh is used per day
Total   : 1210kWh has been used since 12 February, 2022
"""

from datetime import date
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt

# Variables
today = date.today()
datetimeformat = '%d %B, %Y' # Default DD MonthName, YYYY
reading = "read"
date = "date"
sample_size = "7D"
rounding = 2

# Matplot variables
colormap = "jet"
plt.style.use(['ggplot', 'dark_background'])
mpl.rcParams['figure.figsize'] = [8.0, 6.0]
mpl.rcParams['date.converter'] = 'concise'
mpl.rcParams['xtick.direction'] = 'in'
mpl.rcParams['figure.dpi'] = 200
plt.rcParams['axes.xmargin'] = 0

# Read csv file
data = pd.read_csv(
    "energy.csv",
     parse_dates = [date],
     index_col = [date],
     dayfirst = True
)

# Initalise DataFrame
energy = (pd.DataFrame(data,columns = [reading])
            # Resample to daily
            .resample("D")
            # Take the sum
            .sum()
            # Replace 0 will previous row's value
            # The data can be 0 if the provider 'lost' the read
            .replace(to_replace=0, method='ffill')
            # Change resolution to sample_size
            .resample(sample_size)
            # Take the average
            .mean()
            # Round
            .round(rounding)
         )

# Save daily csv for deugging
energy.to_csv('sampled_data.csv', encoding='utf-8')


# Prints a summary of recent energy usage
print(f"The sample size is {sample_size}\n")
print("Most Recent Energy Usage")
print(f"{energy.tail(3)}\n")
print(f"{energy.head(3)}\n")

# Prints stats
average = energy[reading].mean().round(rounding)
maximum = [energy[reading].max(), energy[reading].idxmax().strftime(datetimeformat)]
minimum = [energy[reading].min(), energy[reading].idxmin().strftime(datetimeformat)]
total = int(energy[reading].sum())
start = energy.reset_index()[date][0].strftime(datetimeformat)

print(f"Maximum : {maximum[1]}, {maximum[0]}kWh was used")
print(f"Minimum : {minimum[1]}, {minimum[0]}kWh was used")
print(f"Mean    : An average of {average}kWh is used per day")
print(f"Total   : {total}kWh has been used since {start}")

def plot_save(name: str, title: list[str, str]) -> None:
    plt.xlabel(
        "Date",
        fontsize = 12,
        color = "lightpink"
    )

    plt.ylabel(
        "Energy Consumption (kWh)",
        fontsize = 12,
        color = "lightpink"
    )
    
    plt.title(title[0], fontsize=10, loc="left")
    
    plt.title(title[1], fontsize=10, loc="right")
    
    plt.ylim(0)
    plt.savefig(f"{today}-{name}")
    plt.show()
    plt.close()

# Plot Energy Usage, change these to customize the graph
energy.plot(
    color = 'red',
    linewidth = 2
)

plot_save(
    "line_graph.png",
    [
        "kWh / Day", 
        f"Resolution {sample_size}"
    ]
)

# Scatter plot with interpolated data to look more 'smooth'
energy.resample('30min').mean().interpolate().reset_index().plot.scatter(
    'date',
    'read',
    c='read',
    cmap=colormap,
    s = 3
)

plot_save(
    "scatter_graph_interpolated.png",
    [
        "kWh / Day",
        f"Resolution {sample_size}"
    ]
)
