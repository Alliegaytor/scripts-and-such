""" Remaster of convertToBinary.py 
    While slower for small decimals,
    it gets comparatively faster than
    the other script for larger numbers.
    This script can also process much
    larger decimals without hitting
    the recursion limit.
    
    This is the result of sleep deprivation
    
    E.g. time savings
    input: *some 300 digit int*
    time (original): 0.00074s
    time (new)     : 0.00059s
    
    E.g. time loss
    input: 56
    time (original): 1.1e-05
    time (new)     : 2.4e-05
    
    E.g. length
    input: *some 585 digit int*
    time (original): RecursionError
    time (new)     : 0.0019s
"""

import math


def simplify(num, mod):
    if num % (mod * 2) == 0:
        return simplify(num, mod * 2)
    return mod


def toBinary(num):
    if isinstance(num, int) and num > 0:
        scaleFactor = simplify(num, 1)
        num = num // scaleFactor
                
        power = 0
        binary = []
        
        while 2 ** (power + 1) < num:
            power += 1

        for n in reversed(range(0, power + 1)):
            if num - 2 ** n >= 0:
                num -= 2 ** n
                binary.append("1")
            else:
                binary.append("0")

        print("".join(binary) + "0"*int(math.log(scaleFactor, 2)))

    elif num == 0:
      print("0")
      

def main():
    try:
        toBinary(int(input("Please inout a decimal to convert to binary: ")))
        main()
    except ValueError:
        main()
      
if __name__ == "__main__":
    main()