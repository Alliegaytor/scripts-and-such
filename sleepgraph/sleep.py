import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv('SleepGraph_12-18_01-11.csv', index_col=[0], parse_dates=[0], usecols=['Date','Time Slept'])

temp = pd.to_datetime(df['Time Slept'], format='%H:%M')

# Convert 'Time Slept' to hours
df['Time Slept'] = temp.dt.hour + temp.dt.minute / 60

print(df)

df_7d = df.rolling('7D')

df_7d.mean().plot().set_title("7d sma")

df_7d.max().plot().set_title('7d max')

df_m = df.resample('M').mean().to_period('M')

print(df_m)

df_m.plot().set_title('Monthly avg')

plt.show()

df_30d = df.rolling('30D')

df_30d.mean().plot().set_title('30d sma')
plt.show()
