﻿Function TimeAsleep(byVal vSheet, row As Long) As Double
    Dim sum As Double
    Dim j As Integer
    Dim decColor As Long
    
    decColor = 255
    Set oSheet = getSheet(vSheet)
    
    For j = 1 To 48
        Set oCell = oSheet.getCellByPosition( j, row - 1 )
            if (oCell.CellBackColor = decColor) then
                sum = sum + 1
            endif
    Next j
    
    sum = sum * 30 / 60 / 24

    TimeAsleep = sum
End Function


Function COUNTIFCOLOR(byVal vSheet, range As String, hexColor As String)
	
    range = Trim(range)

    Dim sum As Double
    Dim i As Integer
    Dim j As Integer
    Dim decColor As Long
	
	if (Len(hexColor) <> 6) then
		COUNTIFCOLOR = "ERR: invalid hex code, length <> 6"
		Exit Function
	endif
	
	decColor = hexToDec(hexColor)
    sum = 0.0
    
    Set oSheet = getSheet(vSheet)
    Set oRange = oSheet.getCellRangeByName(range)
    
    For i = 0 To oRange.Rows.getCount() - 1             
        For j = 0 To oRange.Columns.getCount() - 1
            Set oCell = oRange.getCellByPosition( j, i )
                if (oCell.CellBackColor = decColor) then
                    sum = sum + 1
                endif
        Next j
    Next i 
    COUNTIFCOLOR = sum
End Function

Function FirstAsleep(byVal vSheet, row As Long) 
    Dim sum As Integer
    Dim j As Integer
    Dim k As Integer
    Dim x As Integer
    Dim decColor As Long
    Dim decColor_white As Long
    
    decColor = 255
    decColor_nofill = -1
    sum = 0
    Set oSheet = getSheet(vSheet)

    For j = 1 To 48
        Set oCell = oSheet.getCellByPosition( j, row - 1 )
            if (oCell.CellBackColor = decColor) then
            	'Make sure I didn't start the day prior
            	if (j = 1) then
            		if (oSheet.getCellByPosition(48, row - 2).CellBackColor = decColor) then
            			'Check to see if I fall asleep later that day. First by checking to see when I am awake in the day.
            			'TODO: Create 'FirstAsleep' Function
            			For k = 2 to 48
            				Set oCell = oSheet.getCellByPosition(k, row  - 1)
            					if (oCell.CellBackColor = decColor_nofill) then
            					'Then check when I fall asleep
            						For x = k to 48
								        Set oCell = oSheet.getCellByPosition( x, row - 1 )
								            if (oCell.CellBackColor = decColor) then
            									FirstAsleep = x - 1
            								exit function
            								endif
            						next x
            					endif
            			next k
            		    FirstAsleep = "null"
            		    exit function
            		endif
				endif
                exit for
            endif
             sum = sum + 1
    Next j

    FirstAsleep = sum
End Function

'Convert Hex to Dec'
Function hexToDec(hexColor As String) As Long
	Dim i As Integer
	Dim decColor As Long
	
	hexColor = UCase(Trim(hexColor))
	
	For i = 1 To Len(hexColor)
		c = Mid(hexColor, i, 1) 
	    Select Case c
		    Case "0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F"
		    Case Else
		    'COUNTIFCOLOR = "ERR: invalid hex code, invalid hex char"
			Exit Function
		End Select
	Next

	hexToDec = CLng("&H" & hexColor)
End Function

'################### HELPERS FOR ABOVE CELL FUNCTIONS #########################################
Function getSheet(byVal vSheet)
REM Helper for sheet functions. Get cell from sheet's name or position; cell's row-position; cell's col-position
on error goto exitErr
   select case varType(vSheet)
   case is = 8
      if thisComponent.sheets.hasbyName(vSheet) then
         getSheet = thisComponent.sheets.getByName(vSheet)
      else
         getSheet = NULL
      endif
   case 2 to 5
      vSheet = cInt(vSheet)
      'Wow! Calc has sheets with no name at index < 0,
      ' so NOT isNull(oSheet), if vSheet <= lbound(sheets) = CRASH!
      'http://www.openoffice.org/issues/show_bug.cgi?id=58796
      if(vSheet <= thisComponent.getSheets.getCount)AND(vSheet > 0) then
         getSheet = thisComponent.sheets.getByIndex(vSheet -1)
      else
         getSheet = NULL
      endif
   end select
exit function
exitErr:
getSheet = NULL
End Function

Function getSheetCell(byVal vSheet,byVal lRowIndex&,byVal iColIndex%)
dim oSheet
'   print vartype(vsheet)
   oSheet = getSheet(vSheet)
   if varType(oSheet) <>9 then
      getSheetCell = NULL
   elseif (lRowIndex > oSheet.rows.count)OR(lRowIndex < 1) then
      getSheetCell = NULL
   elseif (iColIndex > oSheet.columns.count)OR(iColIndex < 1) then
      getSheetCell = NULL
   else
      getSheetCell = oSheet.getCellByPosition(iColIndex -1,lRowIndex -1)
   endif
End Function
